<?php

use App\Billing\PaymentGateway;
use App\Billing\FakePaymentGateway;
use App\Concert;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class PurchaseTicketsTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();
        $this->paymentGateway = new FakePaymentGateway;
        $this->app->instance(PaymentGateway::class, $this->paymentGateway);

    }

    private function orderTickets($concert, $params)
    {
        $this->json('POST', "/concerts/{$concert->id}/orders", $params);
    }

    private function assertValidationErrors($field)
    {
         $this->assertResponseStatus(422);
        $this->assertArrayHasKey($field, $this->decodeResponseJson());
    }
    
    /** @test */
    public function customer_can_purchase_tickets_to_published_concert()
    {
        $concert = factory(Concert::class)->states('published')->create(['ticket_price' => 3250])->addTickets(3);
        $this->orderTickets($concert, [
            'email' => 'johndoe@example.com',
            'ticket_quantity' => 3,
            'payment_token' => $this->paymentGateway->getValidTestToken(),
            ]);

        $this->assertResponseStatus(201);
        $this->assertEquals(9750, $this->paymentGateway->totalCharges());
        $this->assertTrue($concert->hasOrderFor('johndoe@example.com'));
        $this->assertEquals(3, $concert->ordersFor('johndoe@example.com')->first()->ticketQuantity());

    }

    /** @test */
    public function cannot_purchase_tickets_to_unpublished_concert()
    {
        // $this->disableExceptionHandling();
        $concert = factory(Concert::class)->states('unpublished')->create()->addTickets(3);

        $this->orderTickets($concert, [
            'email' => 'johndoe@example.com',
            'ticket_quantity' => 3,
            'payment_token' => $this->paymentGateway->getValidTestToken(),
            ]);

        $this->assertResponseStatus(404);
        $this->assertFalse($concert->hasOrderFor('johndoe@example.com'));
        $this->assertEquals(0, $this->paymentGateway->totalCharges());
    }

    /** @test */
    public function an_order_is_not_created_if_payment_fails()
    {
        // $this->disableExceptionHandling();
         $concert = factory(Concert::class)->states('published')->create()->addTickets(3);

        $this->orderTickets($concert, [
            'email' => 'johndoe@exmaple.com',
            'ticket_quantity' => 3,
            'payment_token' => 'invalid-payment-token',
            ]);

        $this->assertResponseStatus(422);
        $this->assertFalse($concert->hasOrderFor('johndoe@example.com'));
    }

    /** @test */
    public function cannot_purchase_more_tickets_than_remain()
    {
        $concert = factory(Concert::class)->states('published')->create()->addTickets(50);
        $this->orderTickets($concert, [
            'email' => 'johndoe@exmaple.com',
            'ticket_quantity' => 51,
            'payment_token' => $this->paymentGateway->getValidTestToken(),
            ]);

        $this->assertResponseStatus(422);
        $this->assertFalse($concert->hasOrderFor('johndoe@example.com'));
        $this->assertEquals(0, $this->paymentGateway->totalCharges());
        $this->assertEquals(50, $concert->ticketsRemaining());
    }

    /** @test */
    public function email_is_required_to_purchase_tickets()
    {
        $concert = factory(Concert::class)->states('published')->create();
        $concert->addTickets(3);

        $this->orderTickets($concert, [
            'ticket_quantity' => 3,
            'payment_token' => $this->paymentGateway->getValidTestToken(),
            ]);

        $this->assertValidationErrors('email');
    }


    /** @test */
    public function email_must_be_valid_to_purchase_a_ticket()
    {
        $concert = factory(Concert::class)->states('published')->create();
        $concert->addTickets(3);

        $this->orderTickets($concert, [
            'email' => 'not-an-email',
            'ticket_quantity' => 3,
            'payment_token' => $this->paymentGateway->getValidTestToken(),
            ]);
        $this->assertValidationErrors('email');
    }

    /** @test */
    public function ticket_quantity_is_required_to_purchase_a_ticket()
    {
        $concert = factory(Concert::class)->states('published')->create();
        $concert->addTickets(3);

        $this->orderTickets($concert, [
            'email' => 'johndoe@exmaple.com',
            'payment_token' => $this->paymentGateway->getValidTestToken(),
            ]);
        $this->assertValidationErrors('ticket_quantity');
    }

    /** @test */
    public function ticket_quantity_must_be_atleast_1_to_purchase_a_ticket()
    {
        $concert = factory(Concert::class)->states('published')->create();
        $concert->addTickets(3);

        $this->orderTickets($concert, [
            'email' => 'johndoe@exmaple.com',
            'ticket_quantity' => 0,
            'payment_token' => $this->paymentGateway->getValidTestToken(),
            ]);
        $this->assertValidationErrors('ticket_quantity');
    }

     /** @test */
    public function payment_token_is_required()
    {
        $concert = factory(Concert::class)->states('published')->create();
        $concert->addTickets(3);

        $this->orderTickets($concert, [
            'email' => 'johndoe@exmaple.com',
            'ticket_quantity' => 3,
            ]);
        $this->assertValidationErrors('payment_token');
    }

}
