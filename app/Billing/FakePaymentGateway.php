<?php

namespace App\Billing;

use App\Billing\PaymentGateway;

/**
 * FakePaymentGateway Class
 */
class FakePaymentGateway implements PaymentGateway {
    private $charges;

    function __construct() {
        $this->charges = collect();
    }
    public function getValidTestToken() {
        return 'valid-token';
    }

    public function charge($amount, $token) {
        if ($token != $this->getValidTestToken()) {
            throw new PaymentFailedException;
        }
        $this->charges[] = $amount;
    }

    public function totalCharges() {
        return $this->charges->sum();
    }
}